package com.example.kotlinbasics

fun main() {
    println("lol")
    printMessages()
    fixedCompilerError()
    displaySale()
    getPartyDetails()
    calculateSum(15, 5)
    displayAlert("Chrome OS", "sample@gmail.com")
    displayAlert(emailId ="sample@gmail.com")
    cityReport("Ankara", 27, 31, 82)
    cityReport("Tokyo", 32, 36, 10)
    cityReport("Cape Town", 59, 64, 2)
    cityReport("Guatemala City", 50, 55, 7)

}

fun cityReport(city: String, lowTemp: Int, highTemp: Int, chanceRain: Int)
{
    println("City: $city")
    println("Low temperature: $lowTemp, High temperature: $highTemp")
    println("Chance of rain: $chanceRain%")
    println()
}