package com.example.kotlinbasics

fun calculateSum(firstNumber: Int, secondNumber: Int)
{
    val result = firstNumber + secondNumber
    println("$firstNumber + $secondNumber = $result")
}